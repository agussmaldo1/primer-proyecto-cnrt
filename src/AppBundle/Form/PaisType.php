<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType; 
use Symfony\Component\Form\FormBuilderInterface; 

use Symfony\Component\Form\Extension\Core\Type\TextType; //Agregamos los tipos que vamos a usar 
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PaisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $Builder, array $options)
    {
        $Builder
        -> add('descripcion', TextType::class) 
        -> add('abrev', TextType::class)
        -> add('activo')
        -> add('guardar', SubmitType::class, array('label' => 'Guardar')); //El label lo sacamos de la documentacion, es para poner el nombre que querramos.
    }

}

/*Ya generamos una nueva clase para utilizar en cualquier parte del codigo con las ultimas 2 lineas de Gestion pais, serian:

    $form = $this->createForm(PaisType::class, $pais); //Creamos el constructor
    return $this->render('GestionPais/NuevoPais.html.twig', array('form' => $form->createView()));
*/
   