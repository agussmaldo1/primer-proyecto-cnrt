<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Pais;




class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
      
        // replace this example code with whatever you need
        return $this->render('default/test.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

      /**
     * @Route("/Historia", name="historia")
     */
    public function NosotrosAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/nosotros.html.twig', [ /*Aca deberia poner otra ruta cuando la cree */
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR, 
        ]);
    }

        /**
     * @Route("/Contacto", name="contacto")
     */
    public function ContactoAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/contacto.html.twig', [ /*Aca deberia poner otra ruta cuando la cree */
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR, 
        ]);
    }

           /**
     * @Route("/Paises", name="paises")
     */
    public function PaisesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Pais'); // Capturamos el repositorio de la tabla Pais contra la Base de datos
        $paises = $repository->findAll(); //$paises es donde guardamos el array


        // replace this example code with whatever you need
        return $this->render('default/Paises.html.twig', array('paises'=> $paises)); 

        /* 
        $repository = $this->getDoctrine()->getRepository('AppBundle:Pais'); //en la variable repository cargo la tabla pais
		$bdPaises = $repository->findAll(); //cargo en la variable bdPaises el contenido de la tabla pais (eq a SELECT * FROM Pais)
		return $this->render('paisesactivos.html.twig',array('paises' =>$bdPaises,'action'=>$action)); //junto con el template del form envia un array con todo el contenido de la tabla pais	
        */
    }

            /**
     * @Route("/PaisesActivos", name="paisesactivos")
     */
    public function PaisesActivos (Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Pais'); // Capturamos el repositorio de la tabla Pais contra la Base de datos
        $paises = $repository->findByActivo(1);

        // replace this example code with whatever you need
        return $this->render('default/PaisesActivos.html.twig', array('paises' => $paises));
    }

     /**
     * @Route("/Pais/{id}", name="paisid")
     */
       public function PaisidAction(Request $request, $id=null)
    {
        if (is_null($id)) 
        {
            $response = $this->redirectToRoute('homepage');
        } else
        {
            $paisRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Pais');
            $pais = $paisRepository->find($id);
            $response = $this->render('default/paisid.html.twig', array('pais' => $pais));
        }
        return $response;

    }
        
    
}
