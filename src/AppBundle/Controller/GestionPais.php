<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Pais;
use AppBundle\Form\PaisType; //Le pasamos el nuevo controller 


/**
     * @Route("/Gestion", name="homepage")
     */

class GestionPais extends Controller
{
/**
     * @Route("/NuevoPais", name="nuevopais")
     */
    public function NuevoPaisAction(Request $request)
    {
        $pais = new Pais(); //Generamos el Nuevo pais de la clase PAIS
         
        $form = $this->createForm(PaisType::class, $pais); //Creamos el constructor

        $form->handleRequest($request); //Almacenamos la infromacion

      
        if ($form->isSubmitted() && $form->isValid()) //Si se envio datos y son validos
        {   
            //Meter el Pais en la Base de datos
            $pais = $form->getData(); //Almacenamos la informacion en nuestro objeto de tipo Pais
         //   $pais->setActivo("1");

            // ... perform some action, such as saving the task to the database
            //Almacenamos la informacion del formulario a la BD y hacemos que persista ahí
            $em = $this->getDoctrine()->getManager();
            $em->persist($pais);
            $em->flush();

            return $this->redirectToRoute('paises', array('id' => $pais->getId()));
        }


        // replace this example code with whatever you need
        return $this->render('GestionPais/NuevoPais.html.twig', array('form' => $form->createView()));
           
    }




}